#!/usr/bin/env bash

case $1 in
	install)
		mkdir -p ~/.local/share/gnome-shell/extensions/gtwm@jan.hak
		cp -r src/* ~/.local/share/gnome-shell/extensions/gtwm@jan.hak
		;;
esac