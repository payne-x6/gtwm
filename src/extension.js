/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

'use strict'

/* exported init */

const Main = imports.ui.main;
const Meta = imports.gi.Meta;
const Gio = imports.gi.Gio;
const Shell = imports.gi.Shell;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const vstack = Me.imports.collections.vstack;
const logger = Me.imports.logger;
var Logger = new logger.Logger(logger.Level.TRACE);

let extension = null;

function getSettings() {
	let GioSSS = Gio.SettingsSchemaSource;
	let schemaSource = GioSSS.new_from_directory(
		Me.dir.get_child("schemas").get_path(),
		GioSSS.get_default(),
		false
	);

	let schemaObj = schemaSource.lookup('org.gnome.shell.extensions.gtwm', true);
	if (!schemaObj) {
		throw new Error('cannot find schemas');
	}
	return new Gio.Settings({ settings_schema : schemaObj });
}

function* redrawable(generator) {
	for (const window of generator) {
		if (window.fullscreen || window.minimized) {
			continue;
		}
		yield window;
	}
}

var MonitorState = {
	SINGLETON: 0,
	ONE_STACK: 1,
	TWO_STACKS: 2
};

var WindowState = {
	UNMAXIMIZED: 0,
	MAXIMIZED: 1 
}

class WindowPosition {
	constructor(state, x, y, width, height, monitor) {
		this.state = state;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.monitor = monitor;
	}
}

function redraw_monitor(monitor_idx, windows, storage, offset) {
	Logger.log(logger.Level.TRACE, "Redraw monitor " + monitor_idx);
	storage[offset].monitor = monitor_idx;
	storage[offset].state = WindowState.MAXIMIZED;
	Logger.log(logger.Level.TRACE, "  Main window " + windows[offset]);
	if (windows.length == offset + 1) {
		return 0;
	}

	if (monitor_idx < Main.layoutManager.monitors.length - 1) {
		redraw_monitor(monitor_idx + 1, windows, storage, offset + 1);
	} else {
		for (var i = offset + 1; i < windows.length; ++i) {
			Logger.log(logger.Level.TRACE, "  Secondary window " + windows[i]);
			storage[i].monitor = monitor_idx;
			storage[i].state = WindowState.UNMAXIMIZED;
		}
		return 0;
	}
}

function redraw() {
	Logger.log(logger.Level.TRACE, "Redraw");
	var ws = extension.workspace;
	var windows = Array.from(redrawable(ws.begin.iterator));
	if (windows.length == 0) {
		return;
	}
	var storage = new Array(windows.length);
	for (var i = 0; i < storage.length; ++i) {
		storage[i] = new WindowPosition();
	}
	redraw_monitor(0, windows, storage, 0);
	for (var i = 0; i < windows.length; ++i) {
		Logger.log(logger.Level.TRACE, "Redrawin window...");
		Logger.log(logger.Level.TRACE, "  Monitor " + storage[i].monitor);
		windows[i].move_to_monitor(storage[i].monitor);
		if (storage[i].state == WindowState.MAXIMIZED) {
			Logger.log(logger.Level.TRACE, "  State MAXIMIZED");
			windows[i].maximize(Meta.MaximizeFlags.BOTH);
		} else {
			Logger.log(logger.Level.TRACE, "  State MINIMIZED");
			windows[i].unmaximize(Meta.MaximizeFlags.BOTH);
		}
		Logger.log(logger.Level.TRACE, "  Area [" + storage[i].x + ", " + storage[i].y + ", " + storage[i].width + ", " + storage[i].height + "]" );
		//windows[i].move_resize_frame(false, storage[i].x, storage[i].y, storage[i].width, storage[i].height);
	}
	//for (var monitors_idx = 0; monitor_idx < Main.layoutManager.monitors.length; ++monitors_idx) {
	// if (ws.size == 0) {
	// 	return;
	// }
	// if (ws.size == 1) {
	// 	ws.begin.element.maximize(Meta.MaximizeFlags.BOTH);
	// 	return;
	// }
	// const column_size = ws.size - Main.layoutManager.monitors.length;
	// var first = true;
	// var main_monitors = 0;
	// for (const it of ws.begin.iterator) {
	// 	const wa = Main.layoutManager.getWorkAreaForMonitor(main_monitors);
	// 	const new_y  = wa.height / column_size;
	// 	if (first) {
	// 		var y = wa.y;
	// 	}
		
	// 	if (main_monitors != Main.layoutManager.monitors.length - 1) {
	// 		log("1");
	// 		it.move_to_monitor(main_monitors);
	// 		it.maximize(Meta.MaximizeFlags.BOTH);
	// 		main_monitors++;
	// 	} else {
	// 		it.move_to_monitor(main_monitors);
	// 		it.unmaximize(Meta.MaximizeFlags.BOTH);
	// 		if (first) {
	// 			log("2");
	// 			first = false;
	// 			it.move_resize_frame(false, wa.x, wa.y, wa.width * 0.6, wa.height);
	// 		} else {
	// 			it.move_resize_frame(false, wa.x + (wa.width * 0.6), y, wa.width * 0.4, new_y);
	// 			log("3");
	// 			y += new_y;
	// 		}
	// 	}
	// }
}

function onWindowClosed(window)
{
	extension.workspace.delete(window);
	log("Window closed [" + window + "]");
	redraw();
}

function onWindowCreated(display, window)
{
	if(window.get_window_type() != 0) {
		return;
	}
	window.connect('unmanaged', onWindowClosed);
	extension.workspace.push(window);
	log("Window created [" + window + "]");
	redraw();
}


function onKeyPressed() {
	log("Win");
}

class Extension_GTWM {
	constructor() {
		// this.settings = getSettings();
		this.workspace = new vstack.VStack();
		this._signalWindowCreated = undefined;
	}

	enable() {
		this.settings = getSettings();
		log("my integer: " + this.settings.get_int('my-integer').toString());
		this._signalWindowCreated = global.display.connect('window-created', onWindowCreated);
		let mode = Shell.ActionMode.ALL;
		let flag = Meta.KeyBindingFlags.IGNORE_AUTOREPEAT;
		Main.wm.addKeybinding('focus-next',
			this.settings,
			flag,
			mode,
			() => {
				log("Win");
			}
		);
	}

	disable() {
		global.display.disconnect(this._signalWindowCreated);
		Main.wm.removeKeyBinding("focus-next");
		extension = null;
	}
}

function init()
{
	extension = new Extension_GTWM();
	return extension;
}
