'use strict'

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

var Level = {
	TRACE: "TRACE",
	DEBUG: "DEBUG",
	WARN: "WARN ",
	ERROR: "ERROR",
	FATAL: "FATAL"
};

var Logger = class {
	constructor(level) {
		this.level = level;
	}
};

Logger.prototype.log = function(level, message) {
	log("[" + level + "] " + message);
}