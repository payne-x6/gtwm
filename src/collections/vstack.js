'use strict'

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

function* VStackIterator(begin) {
	for(var it = begin; it.element != null; it = it.next) {
		yield it.element;
	}
}

function* VStackReverseIterator(begin) {
	for(var it = begin; it.element != null; it = it.prev) {
		yield it.element;
	}
}

function* VStackJoin(...generators) {
	for (const generator of generators) {
		for (const it of generator) {
			yield it;
		}
	}
}

class VStackNode {
	constructor(element) {
		this.element = element;
		this.prev = null;
		this.next = null;
	}

	get iterator() {
		return VStackIterator(this);
	}

	get reverse_iterator() {
		return VStackReverseIterator(this);
	}
}

var VStack = class {
	constructor() {
		this.head = new VStackNode(null);
		this.head.prev = this.head;
		this.head.next = this.head;

		this.storage = new Map();
	}

	get begin() {
		return this.head.next;
	}

	get rbegin() {
		return this.head.prev;
	}

	get end() {
		return this.head;
	}

	get size() {
		return this.storage.size;
	}
}

VStackNode.prototype.add = function(count) {
	if (count < 0) {
		return this.subtract(-count);
	}
	var it = this;
	while (count != 0 && it.element != null) {
		it = it.next;
		count--;
	}
	return it;
}


VStackNode.prototype.subtract = function(count) {
	if (count < 0) {
		return this.add(-count);
	}
	var it = this;
	while (count != 0 || it.element != null) {
		it = it.prev
		count--;
	}
	return it;
}

VStack.prototype.push = function(item) {
	if (this.storage.has(item)) {
		return null;
	}
	item.vstack.next = null;
	item.vstack.prev = null;
	var node = new VStackNode(item);
	node.next = this.head.next;
	node.next.prev = node;
	node.prev = this.head;
	node.prev.next = node;
	this.storage.set(item, node);
	return VStackIterator(node);
}

VStack.prototype.delete = function(item) {
	if (!this.storage.has(item)) {
		return null;
	}
	var node = this.storage.get(item);
	node.prev.next = node.next;
	node.next.prev = node.prev;
	var it = node.prev;
	this.storage.delete(item);
	return VStackIterator(it);
}

VStack.prototype.move_top = function(item) {
	if (!this.storage.has(item)) {
		return null;
	}
	var node = this.storage.get(item);
	// Remove from linked list
	node.prev.next = node.next;
	node.next.prev = node.prev;
	// Add to the top of linked list
	node.next = this.head.next;
	node.next.prev = node;
	node.prev = this.head;
	node.prev.next = node;
	return VStackIterator(node);
}

VStack.prototype.move_bottom = function(item) {
	if (!this.storage.has(item)) {
		return null;
	}
	var node = this.storage.get(item);
	// Remove from linked list
	node.prev.next = node.next;
	node.next.prev = node.prev;
	// Add to the bottom of linked list
	node.next = this.head;
	node.next.prev = node;
	node.prev = this.head.prev;
	node.prev.next = node;
	return VStackIterator(node);
}

VStack.prototype.substack = function*(begin, end) {
	for(const it of begin.iterator) {
		if (it === end.element) {
			break;
		}
		yield it;
	}
}
